$(function() {
$("table").tablesorter({ 
        headers: { 
            0: { 
                sorter: false 
            } 
        },
		debug: true 
    }); 
//  $("table").tablesorter({debug: true});

});

function pagging(inputId, itemPerPage)
{
$(inputId).each(function() {
    var currentPage = 0;
    var numPerPage = itemPerPage;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div align="right" style="text-align: right" class="pager pagination"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<a href="javascript:;" class="page-number btn-primary btn"></a>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertAfter($table).find('span.page-number:first').addClass('active');
});
}