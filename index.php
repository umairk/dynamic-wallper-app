<?php
    set_time_limit(0);
    ini_set('memory_limit', '-1');
    session_start();
    include("config.php");
    if(isset($_SESSION['login_user'])){ //name this session one that you set when loggin in the user. 
    }else{ 
    header("Location:login.php"); 
    } 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard - Wallpaper App</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <script src="js/jquery.min.js"></script>
        <script>
            function fnc_disappear(){
            
            $("#error_user").hide();	
            
            }
            
            function validate_form(){
            var title = $(".title").val();
            var tag = $(".tags").val();
            var file=$(".file").val();
            
            if(title.trim() ==""){
                $("#error_user").show();
            $("#error_msg").text("All fields are required!");	
            return false;	
            
            }else if(tag.trim() ==""){
            
            $("#error_user").show();
            $("#error_msg").text("All fields are required!");	
            return false;	
            
            
            }else if(file.trim()==""){
            
            $("#error_user").show();
            $("#error_msg").text("Choose Picture!");	
            return false;	
            
            }
            
            
            }
        </script>
        <style type="text/css">
            #progress {
                font: normal 20px Arial, Helvetica, sans-serif;
                padding: 20px 0px;
            }
            #progress img {
                display: block;
                padding: 20px 0 0;
            }
        </style>
    </head>

    <body>
        <div id="wrapper">

            <!-- Sidebar -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="index.php">Dynamic Wallpapers</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active"><a href="index.php"><i class="icon-dashboard"></i> Dashboard</a></li>
                        <li><a href="category.php"><i class="icon-collapse"></i> Category</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-user">
                        <li class="dropdown user-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo $_SESSION['login_user']; ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                                                    <!--<li><a href="#"><i class="icon-user"></i> Profile</a></li>
                                                    <li class="divider"></li>-->
                                <li><a href="logout.php"><i class="icon-power-off"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Dashboard <small><i>Manage your data</i></small></h1>
                        <ol class="breadcrumb">
                            <li class="active"><i class="icon-dashboard"></i> Dashboard</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#addModal" role="button" data-toggle="modal" class="btn btn-primary" style="float:right; position:relative; bottom:10px">Add Wallpaper</a>
                        <div align="center" style="overflow:hidden" class="modal small hide fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="customModal">
                                <form action="img_save.php" enctype="multipart/form-data" method="post" id="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel" style="margin-top: 5px;
margin-bottom: 10px;">Add Wallpaper</h3>
                                    </div>
                                    <div class="modal-body">
                                        <table cellpadding="10px" id="table">
                                            <tr>
                                                <td>Select Category</td>
                                                <td>
                                                    <select name="choose" class="form-control">
                                                        <?php
                                                            $selc=mysql_query("SELECT * FROM category");
                                                                            while($row=mysql_fetch_array($selc))
                                                            {
                                                                                echo '<option value='.$row['id'].'>'.$row['cat_name'].'</option>';
                                                                                }
                                                        ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td valign="top">Uplode Image</td>
                                                <td><input name="files[]" id="files" class="files" type="file" multiple /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="col-lg-4" id="error_user" style="margin-left:0px; width:80%;position: absolute;
margin-top: 0px;display:none; text-align:center">
                                                        <div class="alert alert-dismissable alert-danger" id="error_msg" style="padding-top:5px; padding-bottom:5px">
                                                            <button type="button" class="close" data-dismiss="alert"></button>
                                                            <strong>Error</strong> <span>Select Image First!</span>
                                                        </div>
                                                    </div></td>
                                            </tr>
                                        </table>
                                        <div align="center" style="display:none" id="progress">
                                            <p>Uploading...</p>
                                            <img style="width:30px" src="477.gif" />
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" aria-hidden="true" href="javascript:;">Cancel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="submit" value="Save" id="input" name="btn_submit" class="btn btn-primary sub">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon-money"></i> Wallpapers View</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <form action="delete.php" id="data_form" method="post">
                                        <input style="position: absolute;right: 145px;top: -43px;" type="submit" name="delete" class="btn btn-primary" id="delete" value="Delete">
                                        <table class="table paginated table-bordered table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th class="{sorter: false}" style="text-align:center">Check All <input type="checkbox" id="check_all" value=""></th>
                                                <th>Image <i class="icon-sort"></i></th>
                                                <th>Name<i class="icon-sort"></i></th>
                                                <th>Date/Time<i class="icon-sort"></i></th>
                                                <th>Title <i class="icon-sort"></i></th>
                                                <th>Tag <i class="icon-sort"></i></th>
												<th>Edit<i class="icon-sort"></i></th>
                                            </tr>
                  </thead>
                                            <tbody>
                                                <?php
                                                    $selc = mysql_query("SELECT category.cat_name,images.id, images.date_time, images.title,images.thumb_url,images.link_path FROM category INNER JOIN images ON category.id=images.cat_id order by images.id DESC");
                                                    
                                                    while($row=mysql_fetch_array($selc))
                                                    {
                                                            echo        
                                                                      '<tr>';
                                                                       echo '<td style="text-align:center;vertical-align:middle"> <input type="checkbox" name="checkbox[]" id="checkbox[]" value="'.$row['id'].'" /></td>'; 
                                                                   echo '<td align="center"><img width="100" src="'.$row['thumb_url'].'"></td>';  
                                                                   echo '<td>'.$row['cat_name'].'</td>';
                                                                   echo '<td>'.$row['date_time'].'</td>';
                                                    
                                                                   echo '<td>'.$row['title'].'</td>';
                                                                   if($row['link_path'] == '--' || $row['link_path'] == "")
                                                                   {
                                                                        echo '<td>'.$row['link_path'].'</td>';
                                                                   } else 
                                                                   {
                                                                        echo '<td><a target="_blank" href="'.$row['link_path'].'">'.$row['link_path'].'</a></td>';
                                                                   }
                                                                   //echo    '<td style="text-align:center"><a style="display: block; padding-top:15px;" onclick="return confirm_box()" href="del_script.php?id='.$row['id'].'"><img src="delet_selected_icon.png"></a></td>';
                                                                    echo    '<td style="text-align:center; vertical-align:middle">
                                                                    <a href="#editModal" role="button" data-key="'.$row['id'].'" data-toggle="modal" class="btn popup btn-primary"><i class="icon-pencil"></i></a></td>';
                                                                    echo '</tr>';
                                                    
                                                    
                                                    } 
                                                    
                                                    //deletion
                                                    if(isset($_POST['delete']))
                                                    {
                                                    $checkbox = $_POST['checkbox']; //from name="checkbox[]"
                                                        $countCheck = count($_POST['checkbox']);
                                                    
                                                        for($i=0;$i<$countCheck;$i++)
                                                        {
                                                            $del_id  = $checkbox[$i];
                                                            $query = mysql_query("SELECT * FROM images WHERE id='$del_id'");
                                                    
                                                            $record = mysql_fetch_array($query);
                                                            $big_image = $record['image_url'];
                                                            $thumb_image = $record['thumb_url'];
                                                    
                                                            unlink($big_image);
                                                            unlink($thumb_image);
                                                            $result = mysql_query("DELETE from images where id = '$del_id'"); //or die(mysqli_error($mysqli));
                                                    
                                                            }
                                                            if($result)
                                                            {	
                                                    
                                                            }
                                                            else
                                                            {
                                                                echo "Error: ".mysql_error();
                                                            }
                                                    }//button
                                                ?>
                                            </tbody>
                                        </table>
                                    </form>

                                    <div align="center" style="overflow:hidden; top:10%" class="modal small hide fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                        <script type="text/javascript">
                                            $(function () {
                                                $(document).on('click', '.popup', function () {
                                                    var popId = $(this).data("key");
                                                    $("#editSrc").attr("src", "edit.php?command=edit&id=" + popId + "");
                                                });
                                                return false;
                                            });
                                            function iframeLoaded() {
                                                var iFrameID = document.getElementById('editSrc');
                                                if (iFrameID) {
                                                    iFrameID.height = "";
                                                    iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
                                                }
                                            }
                                        </script>
                                        <iframe id="editSrc" src="" width="490" onload="iframeLoaded()" allowtransparency="no" frameborder="0"></iframe>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>

        <!-- /#wrapper -->
        <!-- Bootstrap core JavaScript -->
        <script src="js/bootstrap.js"></script>
        <script src="css/bootstrap-fileupload/bootstrap-fileupload.js"></script>
        <!-- Page Specific Plugins -->
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript">
            $(function () {
            
                $("#input").click(function () {
                    var filename = $("#files").val();
                    if (filename == "") {
                        $("#error_user").fadeIn();
                        return false;
                    }
                    var files = $('#files').prop("files");
                    var names = $.map(files, function (val) {
                        var file = val.name.toLowerCase().replace(/^.*\./, '');
                        return file;
                    });
            
                    for (i = 0; i < names.length; i++) {
                        if (names[i] == "jpg" || names[i] == "jpeg" || names[i] == "png") {
            
                        } else {
                            $("#error_user span").text("Invalid Extension!");
                            $("#error_user").fadeIn().delay(2000).fadeOut();
                            return false;
                        }
                    }
            
                    $("#table").hide();
                    $("#progress").show();
                });
            
                /*Multiple file deletion*/
                $("input[id='check_all']").click(function () {
            
                    var inputs = $("input[type='checkbox']");
            
                    for (var i = 0; i < inputs.length; i++) {
                        var type = inputs[i].getAttribute("type");
                        if (type == "checkbox") {
                            if (this.checked) {
                                inputs[i].checked = true;
                            } else {
                                inputs[i].checked = false;
                            }
                        }
                    }
                });
            
                $("input[id='delete']").click(function () {
            
                    var count_checked = $("[name='checkbox[]']:checked").length;
                    if (count_checked == 0) {
                        alert("Please select a product(s) to delete.");
                        return false;
                    }
                    if (count_checked == 1) {
                        return confirm("Are you sure you want to delete these product?");
                    } else {
                        return confirm("Are you sure you want to delete these products?");
                    }
                });
                pagging("table.paginated", 20);
            }); //main
        </script>
    </body>
</html>
