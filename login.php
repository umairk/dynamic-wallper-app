<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Login Page</title>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Add custom CSS here -->
<link href="css/sb-admin.css" rel="stylesheet">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<script src="js/jquery.min.js"></script>
<script>
function fnc_disappear(){
	
$("#error_user").hide();	

}
$(document).ready(function(){
$("#sub").click(function(e){
	e.preventDefault();
	

var user = $(".user").val();
var pass = $(".pass").val();

	
if(user.trim() =="" || pass.trim() ==""){

$("#error_user").show();
$("#error_msg").text("All fields are required!");	
return false;	
}else{
	
$.post("login_script.php",{user:user,pass:pass},function(data){
	
	if(data==1){
	window.location.replace("index.php");	
		
	}else{
	$("#error_user").show();
$("#error_msg").text("Invalid User or Password!");	
		
	}
	
});	

}
	
});
});	

	



</script>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	background: #333333;
}
label {
	font-weight: bold;
	width: 100px;
	font-size: 14px;
	margin:0;
}
.box {
	border: #666666 solid 1px;
}
#input {
	height: 30px;
	width: 250px;
	border-radius: 5px;
	padding-left: 5px;
}
#table tr td
{
	padding: 10px;
}
</style>
</head>
<body>
<div align="center" style="margin-top:200px">
  <div style="width:500px; border: solid 1px #5291c7; background:#FFF;" align="left">
    <div style="background-color:#5291c7; color:#FFFFFF; padding:10px;"><b>Login</b></div>
    <div style="margin:30px;">
      <table id="table">
        <form action="" method="post">
          <tr>
            <td><label>Username  :</label></td>
            <td><input type="text" name="username" class="box user" id="input" onfocus="fnc_disappear()"/></td>
          </tr>
          <tr>
            <td><label>Password  :</label></td>
            <td><input type="password" name="password" class="box pass" id="input" onfocus="fnc_disappear()"/></td>
          </tr>
          <tr>
            <td></td>
            <td style="text-align:right"><a href="mail.php" style="font-size:12px; display: inline-block; position:relative;top:5px">Forget Password</a>&nbsp;&nbsp;
              <input type="submit" value=" Login " class="btn btn-primary" id="sub" style="margin-top:10px;"/></td>
          </tr>
        </form>
      </table>
      <div style="font-size:11px; color:#cc0000; margin-top:10px"></div>
    </div>
  </div>
</div>
<!--error show area-->

<div class="col-lg-4" id="error_user" style="margin-left:695px; width:530px;position: absolute;
margin-top: 5px;display:none; text-align:center">
  <div class="alert alert-dismissable alert-danger" id="error_msg">
    <button type="button" class="close" data-dismiss="alert"></button>
    <strong>Error!</strong> Invalid Username or Password </div>
</div>
</body>
</html>
