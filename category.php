<?php
    require("config.php");
    session_start();
    if(isset($_SESSION['login_user'])){ //name this session one that you set when loggin in the user. 
    }else{ 
    header("Location:login.php"); 
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Category - Wallpaper App</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Add custom CSS here -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <!-- Page Specific CSS -->
        <link href="css/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script>
            function fnc_disappear(){
            
            $("#error_user").hide();	
            
            }
            
            function validate_form(){
            var title = $(".title").val();
            var tag = $(".tags").val();
            var file=$(".file").val();
            
            if(title.trim() ==""){
                $("#error_user").show();
            $("#error_msg").text("All fields are required!");	
            return false;	
            
            }
            
            }
        </script>
    </head>

    <body>
        <div id="wrapper">

            <!-- Sidebar -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="index.php">Dynamic wallpapers</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li><a href="index.php"><i class="icon-dashboard"></i> Dashboard</a></li>
                        <li class="active"><a href="category.php"><i class="icon-collapse"></i> Category</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-user">
                        <li class="dropdown user-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo $_SESSION['login_user']; ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="logout.php"><i class="icon-power-off"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Category <small><i>Manage your data</i></small></h1>
                        <ol class="breadcrumb">
                            <li class="active"><i class="icon-collapse"></i> Category</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#addModal" role="button" data-toggle="modal" class="btn btn-primary" style="float:right; position:relative; bottom:10px; width:120px">Add Category</a>
                        <div align="center" style="overflow:hidden" class="modal small hide fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="customModal">
                                <form action="image_scripts.php" enctype="multipart/form-data" method="post" onsubmit="return validate_form()">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel" style="margin-top: 5px;
margin-bottom: 10px;">Add Category</h3>
                                    </div>
                                    <div class="modal-body">
                                        <table cellpadding="10px" id="table">
                                            <tr>
                                                <td>Name</td>
                                                <td><input type="hidden" name="cat_flag" value="20">
                                                    <input class="form-control title" type="text" name="txt_title_cat" id="input_width" onfocus="fnc_disappear()" style="width:100%"></td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td valign="top">Uplode Image</td>
                                                <td><input id="file" name="file" type="file" onfocus="fnc_disappear()" class="file" style="line-height:15px" />
                  </span>
                                                    <!--data-dismiss="fileupload"-->
                                    </div>

                </td>
                </tr>
                                    <tr>
                                        <td>
                                            <div class="col-lg-4" id="error_user" style="margin-left:0px; width:80%;position: absolute;
margin-top: 0px;display:none; text-align:center">
                                                <div class="alert alert-dismissable alert-danger" id="error_msg" style=" padding-top:5px; padding-bottom:5px">
                                                    <button type="button" class="close" data-dismiss="alert"></button>
                                                    <strong>Error!</strong>Enter All Fields!
                                                </div>
                                            </div></td>
                                    </tr>
            </table>
                            </div>
                            <div class="modal-footer">
                                <a href="category.php">Cancel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="submit" value="Save" id="input" class="btn btn-primary sub">
                            </div>
          </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon-money"></i> Category View</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <?php
                                               echo    '<table class="table table-bordered table-hover table-striped tablesorter">
                                                    <thead>
                                                      <tr>
                                                      	<th style="text-align:center">Select</th>
                                                        <th>Image  <i class="icon-sort"></i></th>
                                                        <th>Name<i class="icon-sort"></i></th>
                                    				  </tr>
                                                    </thead>
                                                      <tbody>';
                                                    $selc=mysql_query("SELECT * FROM category");
                                    while($row=mysql_fetch_array($selc))
                                    {
                                    
                                        $img = $row['thumb_image'];
                                        $img2 = $row['cat_image'];
                                    
                                        $last = substr($img2, -1);
                                        if($last =="."){
                                        $img = "thumbs/default.jpg";	
                                    
                                    
                                        }
                                    
                                            echo        
                                                      '<tr>'; 
													  if($row['id'] == 1 || $row['id'] == 2)
													  {
														  echo '<td></td>';
													  } else {
                                                       echo '<td style="text-align:center;vertical-align:middle"> <form action="cat_del_script.php" method="post"><input type="checkbox" name="checkbox[]" id="checkbox[]" value="'.$row['id'].'" /></td>'; 
													  }
												   echo '<td><img src="'.$img.'"></td>';    
                                    
                                    
                                                    echo     '<td>'.$row['cat_name'].'</td>';
                                    
                                                     echo '</tr>';
                                    
                                    
                                    }
                                    echo "</tbody>
                                                  </table>";
                                ?>
                                <input id="delete" type="submit" onclick="return confirm_box()" class="btn btn-primary" name="delete" value="Delete" style="position: absolute;top: -43px;float: right;right: 145px; width:120px" />
            </form>
                                <script>
									function confirm_box(){
                                    var y = confirm("Are you sure you want to delete?");
                                    if(y==true){
                                    
                                    return true;	
                                    }
                                    return false;	  
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
</div>
        <!-- /#wrapper -->
        <!-- Bootstrap core JavaScript -->
        <script>
            var array = new Array();
            var check;
            
            $(document).ready(function(){
            $("#delete").click(function(){
            if($('input[type="checkbox"]').is(':checked')) {		
            return true;
            
            }else{
            
            alert("Select a value first!");
            return false;	
            }
            });
            });
        </script>
        <script src="js/bootstrap.js"></script>
        <script src="css/bootstrap-fileupload/bootstrap-fileupload.js"></script>
        <!-- Page Specific Plugins -->
        <script src="js/raphael-min.js"></script>
        <script src="js/tablesorter/jquery.tablesorter.js"></script>
        <script src="js/tablesorter/tables.js"></script>
    </body>
</html>