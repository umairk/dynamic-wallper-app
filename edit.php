<?php $command = $_GET['command']; $id = $_GET['id']; include_once('config.php'); 
$rs = mysql_query("select * from images where id = '$id'", $bd);
$rows = mysql_fetch_array($rs);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Edit</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Add custom CSS here -->
<link href="css/sb-admin.css" rel="stylesheet">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<script src="js/jquery.min.js"></script>
<style type="text/css">
.msg
{
	position: absolute;
	top: 75px;
	color: red;
	left: 15px;
}
</style>
</head>

<body style="margin:0">
<div class="editContent">
  <form id="editForm" method="post" name="editForm" action="">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="margin-top: 5px;margin-bottom: 10px;">Edit Data</h3>
  </div>
  <div class="modal-body">
  	<table cellpadding="10px" id="table">
      <tr>
        <td>Title</td>
        <td>
          <input class="form-control title" type="text" name="txt_title" value="<?php echo $rows['title'] ?>" style="width:100%"></td>
      </tr>
      <tr>
        <td>Tag</td>
        <td><input placeholder="http://www.google.com" required class="form-control tags" id="url" type="url" name="txt_tag" value="<?php echo $rows['link_path'] ?>" style="width:100%" ></td>
      </tr>
      <tr>
        <td>Select Category</td>
        <td><select name="choose" class="form-control">
            <?php
$selc=mysql_query("SELECT * FROM category");
                while($row=mysql_fetch_array($selc)) { ?>
                    <option <?php if($row['id'] == $rows['cat_id']) { echo 'selected="selected"'; } ?> value="<?php echo $row['id'] ?>"><?php echo $row['cat_name']; ?></option>
<?php }	?>
          </select></td>
      </tr>
      <tr>
        <td valign="top">Image</td>
        <td><img width="200" src="<?php echo $rows['thumb_url'] ?>" alt=""></td>
      </tr>
      
    </table>
  	 
  </div>
  <div class="modal-footer"> <a class="cancelKey" href="javascript:;">Cancel</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value="Update" name="btn_submit" class="btn btn-primary sub">
  </div>
  </form>
<?php
if(isset($_POST['btn_submit']))
{
	$title = $_POST['txt_title'];
	$tag = $_POST['txt_tag'];
	$cat = $_POST['choose'];
	
	$sql = "update images set title = '$title', link_path = '$tag', cat_id = '$cat' where id = '$id'";
	if(mysql_query($sql, $bd))
	{ ?>
		<script>
			window.top.location = "index.php";
		</script>
	<?php 
	} else 
	{
		echo "Error While Updating!";
	}
}// btn end
?> 
</div>
<script src="js/bootstrap.js"></script>
<script>
$(function(){
	$('.demo-cancel-click').click(function(){return false;});
    $(".close, .cancelKey").click(function(){
			window.parent.$('#editModal').modal('hide');
		return false;
	});
});

function validate() {
        var url = document.getElementById("url").value;
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url) || url == "--") {
            //alert("Url is valid");
            $("#editForm").submit();
			return true;
        } else {
        	alert("Url is not valid!");
        	return false;
		}
}

</script>
</body>
</html>